


let trainerObject = {
  name: "Ash Ketchum",
  age: 10,
  pokemon: ['pikachu', 'Venusaur', 'Charmander','Charmeleon' ],
  friends: {hoenn: ['May','Max'], Kanto: ['Brock','Misty']}

  }

console.log(trainerObject);

trainerObject.talk = function () {
  console.log("Pikachu I choose you!");
};


console.log("Result of dot notation");
console.log(trainerObject.name);
console.log("Result of square bracket notation");
console.log(trainerObject['pokemon']);
console.log("Result of talk method");
trainerObject.talk();

function Pokemon(name, level,health, attack) {

      // Properties
      this.name = name;
      this.level = level;
      this.health = 2 * level;
      this.attack = level;


 //Methods
      this.tackle = function(target) {
          console.log(this.name + ' tackled ' + target.name);
          target.health -= this.attack;
          console.log( target.name + "'s health is now reduced to " + target.health);
          target.health <=0 ? target.faint() : null;
          };
      this.faint = function(){
          console.log(this.name + ' fainted.');
};
}

//'Bulbasaur', 'Venusaur', 'Charmander','Charmeleon'
let pikachu = new Pokemon('pikachu',12,24,12);
let Charmander = new Pokemon('Charmander',12,24,12);
let Venusaur = new Pokemon('Venusaur',12,24,12);
let Charmeleon = new Pokemon('Charmeleon',12,24,12);


console.log(pikachu);
console.log(Charmander);
console.log(Venusaur);
console.log(Charmeleon);

pikachu.tackle(Charmeleon);
console.log(pikachu);
pikachu.tackle(Charmeleon);


/* 

     

          if (target.health < 0) {
          console.log(this.name + "win");
          } else {
            console.log(target.name + " loss");
          }
      }

  }
  // Creates new instances of the "Pokemon" object each with their unique properties
  let pikachu = new PokemonGo("Pikachu", 3);
  let rattata = new PokemonGo('Rattata', 8);

  // Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
  pikachu.tackle(rattata);
  rattata.faint(pikachu);*/